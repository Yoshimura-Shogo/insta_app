require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @user = User.new(name: "Example", user_name: "example",
                    email: "user@example.com",
                    password: "password", password_confirmation: "password")
  end

  test "should be valid" do
    assert @user.valid?
  end

  test "name should not empty and not too long" do
    @user.name = ""
    assert_not @user.valid?
    @user.name = "a" * 51
    assert_not @user.valid?
  end

  test "email should not empty and not too long" do
    @user.email = ""
    assert_not @user.valid?
    @user.email = "a" * 244 + "@example.com"
    assert_not @user.valid?
  end

  test "introduction should not be too long" do
    @user.introduction = "あ" * 300
    assert @user.valid?
    @user.introduction = "a" * 301 
    assert_not @user.valid?
  end

  test "correct email address should be accepted" do
    valid_address = "u_S.e+r@exAple.coM"
    @user.email = valid_address
    assert @user.valid?
  end

  test "incorrect email address should not be accepted" do
    invalid_addresses = %w[user@example,com user@example.
                        user@ex_ample.com]
    invalid_addresses.each do |invalid_address|
      @user.email = invalid_address
      assert_not @user.valid?
    end
  end

  test "email should be unique" do
    dup_user = @user.dup
    @user.save
    dup_user.email.upcase!
    assert_not dup_user.valid?
  end

  test "email address should be downcased before save" do
    original_address = @user.email
    @user.email.upcase!
    @user.save
    assert_equal original_address, @user.reload.email
  end

  test "password should be present(non-blank) and have more than 6 chars" do
    @user.password = @user.password_confirmation= "      "
    assert_not @user.valid?
    @user.password = @user.password_confirmation= "a" * 5
    assert_not @user.valid?
  end

  test " should follow and unfollow a user" do
    michael = users(:michael)
    archer = users(:archer)
    assert_not michael.following?(archer)
    michael.follow(archer)
    assert michael.following?(archer)
    assert archer.followers.include?(michael)
    michael.unfollow(archer)
    assert_not michael.following?(archer)
  end

  test "feed should have the right posts" do
    michael = users(:michael)
    archer = users(:archer)
    lana = users(:lana)
    #フォローしているユーザの投稿を確認
    lana.microposts.each do |micropost|
      assert michael.feed.include?(micropost)
    end
    #自分の投稿を確認
    michael.microposts.each do |micropost|
      assert michael.feed.include?(micropost)
    end
    #フォローしていないユーザの投稿を確認
    archer.microposts.each do |micropost|
      assert_not michael.feed.include?(micropost)
    end
  end

end
