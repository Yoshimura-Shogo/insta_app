require 'test_helper'

class UserEditTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
    @other_user = users(:archer)
  end

  test "unsuccessful edit" do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    patch user_path(@user), params: { user: {name: "", user_name: "",
                              email: "foo@invalid" } }
    assert flash.empty?
    assert_template 'users/edit'
  end

  test "successful edit" do
    log_in_as(@user)
    get edit_user_path(@user)
    patch user_path(@user), params: { user: { name: 'foobar',
                            user_name: 'foo',
                            email: 'user@valid.com' } }
    assert_not flash.empty?
    assert_redirected_to user_url(@user)
    @user.reload
    assert_equal 'foobar', @user.name
    assert_equal 'user@valid.com', @user.email
  end

  test "unsuccessful password edit" do
    log_in_as(@user)
    get "/users/#{@user.id}/edit_password"
    assert_template 'users/edit_password'
    patch "/users/#{@user.id}/update_password", params: { present_password: "",
                          user: { password: "",
                                  password_confirmation: "" } }
    assert_not flash.empty?
    assert_template 'users/edit_password'
  end

  test "successful password edit" do
    log_in_as(@user)
    get "/users/#{@user.id}/edit_password"
    assert_template 'users/edit_password'
    patch "/users/#{@user.id}/update_password", params: { present_password: "password",
                          user: { password: "foobar",
                                  password_confirmation: "foobar" } }
    assert_redirected_to user_url(@user)
    @user.reload
    assert @user.authenticate('foobar')
  end

  test "should redirect edit & password_edit when not logged in" do
    get edit_user_path(@user)
    assert_redirected_to login_path
    assert_not flash.empty?
    get "/users/#{@user.id}/edit_password"
    assert_redirected_to login_path
    assert_not flash.empty?
  end

  test "should redirect update & update_password" do
    patch user_path(@user), params:{}
    assert_redirected_to login_path
    assert_not flash.empty?
    patch "/users/#{@user.id}/update_password", params:{}
    assert_redirected_to login_path
    assert_not flash.empty?
  end

  test "should redirect edit & edit_password when logged in as the wrong" do
    log_in_as(@other_user)
    get edit_user_path(@user)
    assert_redirected_to root_path
    assert_not flash.empty?
    get "/users/#{@user.id}/edit_password"
    assert_redirected_to root_path
    assert_not flash.empty?
  end

  test "should redirect update & update_password when logged in the wrong" do
    log_in_as(@other_user)
    patch user_path(@user), params:{}
    assert_redirected_to root_path
    assert_not flash.empty?
    patch "/users/#{@user.id}/update_password", params:{}
    assert_redirected_to root_path
    assert_not flash.empty?
  end
end
