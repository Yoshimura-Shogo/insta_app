require 'test_helper'

class MicropostsTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
    @other_user = users(:archer)
    @picture = fixture_file_upload('test/fixtures/test.jpg', 'image/jpg')
    log_in_as(@user)
    post microposts_path, params: { micropost: { picture: @picture } }
    @micropost = Micropost.first
  end

  test "should be valid" do
    assert @micropost.valid?
  end

  test "picture should be present" do
    @micropost.picture = ""
    assert_not @micropost.valid?
  end

  test "user_id should be present" do
    @micropost.user_id = ""
    assert_not @micropost.valid?
  end

  test "associated microposts should be destroyed" do
    assert_difference 'Micropost.count', -1 do
      @user.destroy
    end
  end

  test "should not include delete when logged in as other user" do
    log_in_as(@other_user)
    get user_path(@user)
    assert_select 'a', text: '投稿を削除', count: 0
  end

  test "should display correct micropost count" do
    get root_path
    assert_match "1件の投稿", response.body
    log_in_as(@other_user)
    get root_path
    assert_match "0件の投稿", response.body
  end
end
