require 'test_helper'

class UsersShowTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
    @other_user = users(:archer)
  end

  test "show should not include delete when logged in as wrong user" do
    log_in_as(@other_user)
    get user_path(@user)
    assert_select 'a', text: "削除", count: 0
    assert_select 'a', text: "編集", count: 0
  end

  test "show include correct anchor" do
    log_in_as(@user)
    get user_path(@user)
    assert_select 'a', text: "削除"
    assert_select 'a', text: "編集"
  end

  test "profile display" do
    log_in_as(@user)
    get user_path(@user)
    assert_template 'users/show'
    assert_select 'title', full_title(@user.name)
    assert_select 'img.profile-image'
  end
end
