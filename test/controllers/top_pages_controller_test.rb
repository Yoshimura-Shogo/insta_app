require 'test_helper'

class TopPagesControllerTest < ActionDispatch::IntegrationTest
  def setup
    @base_title = 'Insta_app'
  end

  test "should get home" do
    get root_path
    assert_response :success
    assert_select 'title', 'Insta_app'
  end
end
