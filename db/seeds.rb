#ユーザ
User.create!(name: "Minase Inori", user_name: "inorin",
            email: "minase@example.com",
            password: "minase", password_confirmation: "minase",
          profile_image: open("#{Rails.root}/db/fixtures/profile_image.jpg"))

99.times do |n|
  name = Faker::Name.name
  user_name = name.downcase
  email = "example-#{n}@railstutorial.org"
  password = "password"
  User.create!(name: name, user_name: user_name,
                email: email, password: password,
              password_confirmation: password)
end

users = User.order(:created_at).take(6)
1.upto(3) do |n|
  users.each { |user| user.microposts.create!(picture: open("#{Rails.root}/db/fixtures/sample#{n}.jpg")) }
end

#リレーションシップ
users = User.all
user = User.first
following = users[2..50]
followers = users[3..40]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }
