module SessionsHelper

  #ユーザーをログイン状態にする
  def login(user)
    session[:user_id] = user.id
  end

  #ログインユーザがいれば返す
  def current_user
    if session[:user_id]
      @current_user ||= User.find_by(id: session[:user_id])
    end
  end

#ログインしてればtrue,していなければfalseを返す
  def logged_in?
    !current_user.nil?
  end

#現ユーザをログアウトする
  def logout
    session.delete(:user_id)
    @current_user = nil
  end

#渡されたユーザがログインユーザならばtrueを返す
  def current_user?(user)
    current_user == user
  end
end
