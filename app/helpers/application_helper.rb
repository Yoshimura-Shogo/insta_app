module ApplicationHelper
  #ページごとのタイトルを返す
  def full_title(page_title = "")
    base_title = 'Insta_app'
    if page_title.empty?
      base_title
    else
      page_title + ' | ' + base_title
    end
  end

  #プロフィール画像を返す
  def set_profile_image(user, size: 80)
    if user.profile_image?
      image_tag user.profile_image,
                    size: size, alt: user.name, class: "profile-image"
    else
      image_tag 'default_profile_image.jpg',
                    size: size, alt: user.name, class: "profile-image"
    end
  end
end
