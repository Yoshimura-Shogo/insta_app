class CommentsController < ApplicationController
  before_action :logged_in_user

  def create
    @micropost = Micropost.find(params[:micropost_id])
    @comment = @micropost.comments.build(comment_params)
    @comment.user = current_user
    if @comment.save
      flash[:success] = "コメントを投稿しました"
      redirect_to micropost_path(@micropost)
    else
      render 'microposts/show'
    end
  end

  def destroy
    @micropost = Micropost.find(params[:id])
    @comment = Comment.find(params[:comment_id])
    @comment.destroy
    flash[:success] = "コメントを削除しました"
    redirect_to @micropost
  end

  private

    def comment_params
      params.require(:comment).permit(:content)
    end
end
