class MicropostsController < ApplicationController
  before_action :logged_in_user
  before_action :correct_user, only: :destroy

  def new
    @micropost = current_user.microposts.build
  end

  def create
    @micropost = current_user.microposts.build(micropost_params)
    if @micropost.save
      flash[:success] = "写真を投稿しました。"
      redirect_to current_user
    else
      render 'microposts/new'
    end
  end

  def destroy
    @micropost.destroy
    flash[:success] = "写真を削除しました"
    redirect_to current_user
  end

  def show
    @micropost = Micropost.find(params[:id])
    @comment = @micropost.comments.build(user_id: current_user.id) if current_user
    @comments = @micropost.comments.paginate(page: params[:page])
  end

  private

    def micropost_params
      params.fetch(:micropost, {}).permit(:picture)
    end

#beforeフィルター

#投稿写真がログインユーザーのものかを検証
    def correct_user
      @micropost = Micropost.find_by(id: params[:id])
      redirect_to root_url unless @micropost.user == current_user
    end
end
