class UsersController < ApplicationController
  before_action :logged_in_user, except: [:new, :create]
  before_action :correct_user, only: [:edit, :update,
                                      :edit_password, :update_password, :destroy]

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      login @user
      flash[:success] = "登録完了しました。"
      redirect_to @user
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if @user.update_attributes(user_params)
      flash[:success] = "プロフィールを変更しました"
      redirect_to @user
    else
      render 'edit'
    end
  end

  def edit_password
  end

  def update_password
    if check_present_password
      flash[:success] = "パスワードを変更しました"
      redirect_to @user
    else
      flash[:danger] = "やり直してください"
      render 'edit_password'
    end
  end

  def show
    @user = User.find(params[:id])
    @microposts = @user.microposts.paginate(page: params[:page])
  end

  def index
    @users = User.paginate(page: params[:page])
  end

  def destroy
    @user.destroy
    logout
    flash[:success] = "アカウントを削除しました"
    redirect_to root_url
  end

  def following
    @title = "フォローユーザー"
    @user = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end

  def followers
    @title = "フォロワー"
    @user = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end

  private

    def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :user_name, :profile_image,
                                   :introduction, :web_url)
    end

#入力値と現在のパスワードがあっていればtrueを返す
    def check_present_password
      @user.authenticate(params[:present_password]) &&
      params[:user][:password] != nil &&
      @user.update_attributes(user_params)
    end

#beforeフィルター

#正しいユーザーでなければルートへリダイレクト
    def correct_user
      @user = User.find(params[:id])
      unless current_user?(@user)
        redirect_to root_url
      end
    end
end
