class Micropost < ApplicationRecord
  belongs_to :user
  has_many :comments, dependent: :destroy
  validates :picture, presence: true
  validates :user_id, presence: true
  validate :picture_size
  default_scope -> { order(created_at: :desc) }
  mount_uploader :picture, PictureUploader

  private

#アップロードされた画像のサイズをバリデート
    def picture_size
      if picture.size > 5.megabytes
        errors.add(:picture, "should be less then 5Mb")
      end
    end
end
