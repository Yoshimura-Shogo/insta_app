class User < ApplicationRecord
  before_save :email_downcase
  REGEX_ADDRESS_VALID = /\A[\w+\-.]+@[a-z]+\.[a-z]+\z/i
  validates :name, presence: true, length: { maximum: 50 }
  validates :user_name, presence: true, length: { maximum: 50 },
            uniqueness: true
  validates :email, presence: true, length: { maximum: 255 },
            format: { with: REGEX_ADDRESS_VALID },
            uniqueness: { case_sensitive: false }
  validates :password, presence: true, length: { minimum: 6 }, allow_nil: true
  validates :introduction, length: { maximum: 300 }
  validates :web_url, length: { maximum: 50 }
  validate :profile_image_size
  has_secure_password
  has_many :microposts, dependent: :destroy
  has_many :comments
  has_many :active_relationships, class_name:  "Relationship",
                                  foreign_key: "follower_id",
                                  dependent:   :destroy
  has_many :following, through: :active_relationships, source: :followed
  has_many :passive_relationships, class_name:  "Relationship",
                                  foreign_key: "followed_id",
                                  dependent:   :destroy
  has_many :followers, through: :passive_relationships, source: :follower
  mount_uploader :profile_image, ProfileImageUploader

#与えられた文字列のダイジェストを返す
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ?
                              BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

#ログインユーザのfeedを返す
  def feed
    following_ids = "SELECT followed_id FROM relationships
                      WHERE follower_id = :user_id"
    Micropost.where("user_id IN (#{following_ids})
                                  OR user_id = :user_id", user_id: id)
  end

#ユーザをフォローする
  def follow(other_user)
    following << other_user
  end

#フォローを解除する
  def unfollow(other_user)
    active_relationships.find_by(followed_id: other_user.id).destroy
  end

#ログインユーザがフォローしていたらtrueを返す
  def following?(other_user)
    following.include?(other_user)
  end

  private
    def email_downcase
      self.email.downcase!
    end

#アップロードされた画像のサイズをバリデート
    def profile_image_size
      if profile_image.size > 5.megabytes
        errors.add(:profile_image, "should be less then 5Mb")
      end
    end
end
